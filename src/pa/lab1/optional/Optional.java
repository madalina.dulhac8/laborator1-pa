package pa.lab1.optional;



public class Optional {


    /**
     * In this function, I created an adjacency matrix using a
     * math formula which generates a random number (either 0
     * or 1)
     * I used a condition so when we have i=j, then the matrix
     * component will be equal to 0 because in a graph it doesn't
     * exist an edge from a vertex to itself
     * @param nr
     * @return the adjacency matrix
     */

    private static int[][] randomAdjacencyMatrix(int nr) {
        int[][] adjacencyMatrix = new int[nr][nr];
        for (int i = 0; i < nr; i++) {
            for (int j = 0; j < nr; j++) {
                if (i == j) {
                    adjacencyMatrix[i][j] = 0;
                } else {
                    adjacencyMatrix[i][j] = adjacencyMatrix[j][i] = (int) Math.round(Math.random());
                }
            }
        }
        return adjacencyMatrix;
    }

    /**
     * This function displays the random adjacency matrix
     * that I generated above
     * @param adjacencyMatrix
     */

    private static void displayAdjacencyMatrix(int[][] adjacencyMatrix) {
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                System.out.print(adjacencyMatrix[i][j] + "  ");
            }
            System.out.println("\n");
        }
    }

    /**
     * This function will apply the DFS algorithm on the given graph,
     * starting from the "vertex" point and will return the
     * array of visited vertices
     * @param vertex
     * @param adjacencyMatrix
     * @param visited
     * @return
     */

    public static int[] algDFS(int vertex, int[][] adjacencyMatrix, int[] visited) {
        visited[vertex] = 1;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[vertex][i] == 1 && visited[i] == 0 && vertex != i) {
                algDFS(i, adjacencyMatrix, visited);
            }
        }
        return visited;
    }

    /**
     * This function will reveal if a given graph is connected
     * or not by starting from a vertex and using the DFS algorithm
     * to see if we can reach any other vertex from the graph
     * @param adjacencyMatrix
     * @param visited 1 means visited, 0 means not visited
     * @return
     */

    public static boolean checkConnected(int[][] adjacencyMatrix, int[] visited) {
        visited = algDFS(0, adjacencyMatrix, visited);
        for (int i = 0; i < visited.length; i++) {
            if (visited[i] == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * First, we display the component that has been visited
     * and then we visit the rest of the vertices and we will
     * recursively display the components on the screen
     * @param adjacencyMatrix
     * @param visited
     */

    public static void displayComponents(int[][] adjacencyMatrix, int[] visited) {
        System.out.print(" [ ");
        for (int i = 0; i < visited.length; i++) {
            if (visited[i] == 1) {
                System.out.print(i + " ");
                visited[i]++;
            }
        }
        System.out.print(" ] ");

        for (int i = 0; i < visited.length; i++) {
            if (visited[i] == 0) {
                visited = algDFS(i, adjacencyMatrix, visited);
                displayComponents(adjacencyMatrix, visited);
            }
        }
    }

    /**
     * This function will display the time in nanoseconds
     * @param start
     */

    public static void displayTime(long start){
        long end = System.nanoTime();
        long time = end - start;
        System.out.println("The time in nanoseconds: " + time);
    }

    public static void main(String[] args) {
        long time = System.nanoTime();

        int number = Integer.parseInt(args[0]);
        System.out.println("The number is: " + number);

        if (number > 30_000){
            displayTime(time);
        }
        else {

            int[][] adjacencyMatrixMtx = randomAdjacencyMatrix(number);
            displayAdjacencyMatrix(adjacencyMatrixMtx);

            int[] visited = new int[adjacencyMatrixMtx.length];

            boolean result = checkConnected(adjacencyMatrixMtx, visited);
            if (result) {
                System.out.println("The graph is connected!");
            }
            System.out.println("The graph is not connected!");

            if (!result) {
                displayComponents(adjacencyMatrixMtx, visited);
            } else {
                System.out.println("IDK!");
            }
            }
        }
    }