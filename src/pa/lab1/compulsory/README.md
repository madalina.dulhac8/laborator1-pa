# Laboratorul 1 - PA 

## Requisite

Write a Java application that implements the following operations:
1. Display on the screen the message "Hello World!". Run the application. If it works, go to step 2 :)
2. Define an array of strings languages, containing {"C", "C++", "C#", "Python", "Go", "Rust", "JavaScript", "PHP", "Swift", "Java"}
3. Generate a random integer n: int n = (int) (Math.random() * 1_000_000);
4. Compute the result obtained after performing the following calculations:
    * multiply n by 3;
    * add the binary number 10101 to the result;
    * add the hexadecimal number FF to the result;
    * multiply the result by 6;
5. Compute the sum of the digits in the result obtained in the previous step. This is the new result. While the new result has more than one digit, continue to sum the digits of the result.
6. Display on the screen the message: "Willy-nilly, this semester I will learn 

##Solution Compulsory
*  I created three functions which helped me manipulate the random number
*  The output is: "Willy-nilly, this semester I will learn Java"

##Solution Optional
* I generated the random adjacency matrix and I displayed it on the screen
* After that, I used DFS to obtain an array of visited vertices 
* Using the array and the DFS algorithm, I checked if the graph is connected or not
* If it is not connected, I created a function to display the connected components 