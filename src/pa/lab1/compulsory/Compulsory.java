package pa.lab1.compulsory;



public class Compulsory {


    /**
     * Funcția dată generează un număr natural random cuprins între 0 și 1.000.000;
     * @return random number
     */

    private static int generateRandomNumber(){
        return (int) (Math.random() * 1_000_000);
    }

    /**
     * Parametrul n, număr natural, este prelucrat în cadrul acestei funcții;
     * Numărul n este înmulțit cu 3, la rezultatul căruia se adaugă numărul binar 10101;
     * Se adaugă numărul hexazecimal FF;
     * Pentru ambele calcule, am utilizat funcția Integer.parseInt() care primește un număr natural
     * și oferă reprezentarea numărului în baza aleasă prin "radix";
     * Numărul obținut la pasul anterior se înmulțește cu 6;
     * Se returnează n rezultat în urma calculelor.
     */
      private static int processingNumber(int n){
          n = n * 3;
          n = n + Integer.parseInt("10101",2);
          n = n + Integer.parseInt("FF",16);
          n = n * 6;
          return n;
      }

    /**
     * Am creat această funcție pentru a face suma cifrelor lui n;
     * Am utilizat două variabile:
     * una temporară pentru suma cifrelor numărului n;
     * una pentru suma cifrelor în cazul în care suma temporară e mai mare decât 9;
     * Se returnează suma finală, un număr <= 9.
     */

      private static int sumOfDigits(int n){
          int temporary_sum = 0;
          int sum_of_digits = 0;
          while(n != 0){
              temporary_sum = temporary_sum + n % 10;
              n = n / 10;
          }
          if(temporary_sum > 9) {
              while (temporary_sum != 0) {
                  sum_of_digits = sum_of_digits + temporary_sum % 10;
                  temporary_sum = temporary_sum / 10;
              }
          }
          else{
              sum_of_digits = temporary_sum;
          }
          return sum_of_digits;
      }


    static public void main(String[] args) {

        System.out.println("Hello, world!");

        String[] languages = {"C", "C++", "C#", "Python", "Go", "Rust", "JavaScript", "PHP", "Swift", "Java"};

        int randomNumber = generateRandomNumber();
        System.out.println("Numărul generat este: " + randomNumber);

        randomNumber = processingNumber(randomNumber);
        randomNumber = sumOfDigits(randomNumber);

        System.out.println("Willy-nilly, this semester I will learn " + languages[randomNumber]);
    }
}